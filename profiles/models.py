import uuid

from django.utils.translation import gettext_lazy as _
from django.db import models

from base.models import BaseModelMixin
from users.models import User


class Profile(BaseModelMixin):
    class SexType(models.TextChoices):
        MALE = "male", _("Male")
        FEMALE = "female", _("Female")
        NON_BINARY = "non_Binary", _("Non Binary")
        OTHER = "other", _("Other")
        PREFER_NOT_TO_SAY = "prefer_not_to_say", _("Prefer not to say")

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    birth_date = models.DateTimeField(null=True)
    dni = models.IntegerField(primary_key=True)
    sex_type = models.CharField(null=True, choices=SexType.choices, max_length=20)
    wallet_id = models.UUIDField(default=uuid.uuid4, unique=True)

    def __str__(self):
        return self.user.get_full_name()
