import pytest

from rest_framework.test import APIClient

from tests.api.v1.factories import UserFactory, ProfileFactory


@pytest.fixture
def admin_user():
    user = UserFactory(
        username="admin",
        first_name="Super",
        last_name="Admin",
        email="admin@mail.com",
        is_staff=True,
        is_active=True,
        is_superuser=True
    )
    user.set_password("Mysuperpass")
    user.save()
    return user


@pytest.fixture
def client_api(admin_user):
    api_client = APIClient()
    api_client.force_authenticate(user=admin_user)
    return api_client


@pytest.fixture
def final_user():
    user = UserFactory()
    ProfileFactory(user=user)
    return user
