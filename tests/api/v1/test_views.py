import uuid
from dateutil.relativedelta import relativedelta
import pytest

from django.contrib.auth import get_user_model
from django.urls import reverse
from rest_framework import status
from profiles.models import Profile


User = get_user_model()


@pytest.mark.django_db
def test_list_user(client_api, final_user):
    url = reverse("user-list")
    response = client_api.get(url)
    assert response.status_code == status.HTTP_200_OK
    data = response.json()
    assert len(data.get("results")) == 1


@pytest.mark.django_db
def test_create_user(client_api):
    data = {
        "wallet_id": uuid.uuid4(),
        "email": "fake@mail.com",
        "name": "Julio",
        "last_name": "Falcioni",
        "sex_type": "male",
        "dni": "20999888",
        "birth_date": "1990-01-10"
    }
    url = reverse("user-list")
    response = client_api.post(url, data=data, json=True)
    assert response.status_code == status.HTTP_201_CREATED


@pytest.mark.django_db
def test_update_user(client_api, final_user):
    old_birth_date = final_user.profile.birth_date
    data = {
        "email": final_user.email,
        "name": f"fake_{final_user.first_name}",
        "last_name": final_user.last_name,
        "sex_type": final_user.profile.sex_type,
        "birth_date": (final_user.profile.birth_date - relativedelta(years=4)).isoformat()
    }
    url = reverse("user-detail", kwargs={"pk": final_user.pk})
    response = client_api.put(url, data=data, json=True)
    assert response.status_code == status.HTTP_200_OK
    final_user.refresh_from_db()
    assert final_user.profile.birth_date != old_birth_date
    assert final_user.first_name == data.get("name")


@pytest.mark.django_db
def test_delete_user(client_api, final_user):
    url = reverse("user-detail", kwargs={"pk": final_user.pk})
    response = client_api.delete(url)
    assert response.status_code == status.HTTP_204_NO_CONTENT
    assert not User.objects.filter(pk=final_user.pk).exists()
    assert not Profile.objects.filter(user=final_user).exists()
