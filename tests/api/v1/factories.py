import factory
from django.contrib.auth import get_user_model
import datetime
from profiles.models import Profile


User = get_user_model()


class UserFactory(factory.django.DjangoModelFactory):
    username = factory.Faker("user_name")
    first_name = factory.Faker("first_name")
    last_name = factory.Faker("last_name")
    email = ""

    class Meta:
        model = User
        django_get_or_create = ('username', )


class ProfileFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)
    sex_type = "female"
    dni = factory.Sequence(lambda x: x + 1)
    birth_date = factory.Faker("date_time", tzinfo=datetime.timezone.utc)

    class Meta:
        model = Profile
