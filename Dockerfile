FROM python:3.11.4-slim-bullseye

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY start /start
RUN sed -i 's/\r$//g' /start
RUN chmod +x /start

WORKDIR /code

COPY . .
