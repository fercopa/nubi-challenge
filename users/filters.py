from django_filters import rest_framework as filters
from django.contrib.auth import get_user_model

User = get_user_model()


SORT_CHOICES = (
    ("wallet_id", "wallet_id"),
    ("email", "email"),
    ("name", "name"),
    ("last_name", "last_name"),
    ("sex_type", "sex_type"),
    ("dni", "dni"),
    ("birth_date", "birth_date"),
    ("created_at", "created_at"),
    ("-wallet_id", "-wallet_id"),
    ("-email", "-email"),
    ("-name", "-name"),
    ("-last_name", "-last_name"),
    ("-sex_type", "-sex_type"),
    ("-dni", "-dni"),
    ("-birth_date", "-birth_date"),
    ("-created_at", "-created_at"),
)


class UserFilter(filters.FilterSet):
    sortBy = filters.ChoiceFilter(choices=SORT_CHOICES, method="filter_sort")
    name = filters.CharFilter(field_name="first_name")
    wallet_id = filters.UUIDFilter(field_name="profile__walliet_id")
    sex_type = filters.CharFilter(field_name="profile__sex_type")
    dni = filters.NumberFilter(field_name="profile__dni")
    birth_date = filters.DateTimeFilter(field_name="profile__birth_date")

    class Meta:
        model = User
        fields = ("last_name", "email", "created_at")

    def filter_sort(self, queryset, name, value):
        sufix = "-" if value.startswith("-") else ""
        value = value.strip("-")
        value = "first_name" if value == "name" else value
        user_fields = ("first_name", "last_name", "email")
        order_by = value if value in user_fields else f"profile__{value}"
        return queryset.order_by(f"{sufix}{order_by}")
