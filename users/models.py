from django.contrib.auth.models import AbstractUser

from base.models import BaseModelMixin, BaseUserManager


class User(AbstractUser, BaseModelMixin):
    """ User with logic delete """
    objects = BaseUserManager()
