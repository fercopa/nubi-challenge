import os
from django.conf import settings
from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model

User = get_user_model()


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        master_password = os.getenv("DJANGO_ADMIN_PASSWORD", "admin")
        if not User.objects.all().exists():
            for username, email in settings.ADMINS:
                user = User.objects.create_superuser(username, email, master_password)
                print(f"Superuser {user.username} created sucessful")
