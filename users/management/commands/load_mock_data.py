import os
import requests
import logging
from django.core.management.base import BaseCommand
from api.v1.serializers import UserSerializer


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        user_info = self.get_users_info()
        if not user_info:
            return
        amount = 0
        for user in user_info:
            user_data = self.get_user_data(user)
            user_serializer = UserSerializer(data=user_data)
            if user_serializer.is_valid():
                try:
                    user_serializer.save()
                    amount += 1
                except Exception:
                    logging.warning(f"User not created with this data: {user}. It already exists?")
        logging.info(f"Imported {amount} users!")

    @staticmethod
    def get_user_data(data):
        profile = {
            "sex_type": data.get("sex_type"),
            "dni": data.get("dni"),
            "birth_date": data.get("birth_date"),
            "wallet_id": data.get("wallet_id"),
        }
        user_data = {
            "email": data.get("email"),
            "first_name": data.get("name"),
            "last_name": data.get("last_name"),
            "profile": profile,
        }
        return user_data

    @staticmethod
    def get_users_info():
        url = os.getenv("USER_API_MOCK")
        if not url:
            return
        response = requests.get(url)
        return response.json()
