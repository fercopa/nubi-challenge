from django.contrib.auth import get_user_model

from rest_framework.viewsets import ModelViewSet
from rest_framework.pagination import PageNumberPagination
from django_filters import rest_framework as filters

from api.v1.serializers import UserDtoSerializer
from users.filters import UserFilter


User = get_user_model()


class CustomPagiantion(PageNumberPagination):
    page_size = 10
    page_size_query_param = "limit"


class UserView(ModelViewSet):
    serializer_class = UserDtoSerializer
    filter_backends = (filters.DjangoFilterBackend, )
    filterset_class = UserFilter
    pagination_class = CustomPagiantion

    def get_queryset(self):
        queryset = User.objects.prefetch_related(
            "profile"
        ).filter(profile__isnull=False).all()
        return queryset

    def perform_destroy(self, instance):
        instance.profile.delete()
        super().perform_destroy(instance)
