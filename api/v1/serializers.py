from copy import deepcopy
from django.contrib.auth import get_user_model

from rest_framework import serializers

from profiles.models import Profile


User = get_user_model()


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ("dni", "birth_date", "sex_type", "wallet_id")


class UserSerializer(serializers.ModelSerializer):
    username = serializers.CharField(required=False)
    profile = ProfileSerializer()

    class Meta:
        model = User
        fields = ("username", "first_name", "last_name", "email", "profile")

    def create(self, validated_data):
        profile_data = validated_data.pop("profile", None)
        username = validated_data.pop("username", None) or profile_data.get("dni")
        user = User.objects.create_user(username=username, **validated_data)
        Profile.objects.create(user=user, **profile_data)
        return user

    def update(self, instance, validated_data):
        profile_data = validated_data.pop("profile", None)
        profile_serializer = ProfileSerializer(instance=instance.profile, data=profile_data, partial=True)
        if profile_serializer.is_valid(raise_exception=True):
            profile_serializer.save()
        return super().update(instance, validated_data)


class UserDtoSerializer(serializers.Serializer):
    wallet_id = serializers.UUIDField(required=False)
    email = serializers.EmailField(required=False)
    name = serializers.CharField()
    last_name = serializers.CharField(required=False)
    sex_type = serializers.CharField()
    dni = serializers.IntegerField(required=False)
    birth_date = serializers.DateTimeField(required=False, allow_null=True)

    def create(self, validated_data):
        user_data = self.get_user_data(validated_data)
        user_serializer = UserSerializer(data=user_data)
        if user_serializer.is_valid(raise_exception=True):
            user_serializer.save()
        return user_serializer.instance

    def update(self, instance, validated_data):
        user_data = self.get_user_data(validated_data)
        user_serializer = UserSerializer(instance=instance, data=user_data, partial=True)
        if user_serializer.is_valid(raise_exception=True):
            user_serializer.save()
        return user_serializer.instance

    def get_user_data(self, data):
        data = deepcopy(data)
        user_data = dict()
        if "name" in data:
            data["first_name"] = data.pop("name")
        for key in UserSerializer.Meta.fields:
            if key in data:
                user_data[key] = data.get(key)
        user_data.update(profile=self.get_profile_data(data))
        return user_data

    @staticmethod
    def get_profile_data(data):
        profile = dict()
        for key in ProfileSerializer.Meta.fields:
            if key in data:
                profile[key] = data.get(key)
        return profile

    def to_representation(self, instance):
        data = {
            "email": instance.email,
            "name": instance.first_name,
            "last_name": instance.last_name,
        }
        if hasattr(instance, "profile"):
            data["wallet_id"] = instance.profile.wallet_id,
            data["sex_type"] = instance.profile.sex_type,
            data["dni"] = instance.profile.dni,
            data["birth_date"] = instance.profile.birth_date,
        return data
