# nubi-challenge


## Wiki
Para ver los detalles lógica y tecnologías aplicada, existe una Wiki que cuenta con más información

[Wiki Home](https://gitlab.com/fercopa/nubi-challenge/-/wikis/Home)

## Instrucciones
Esta aplicación está basda en Python 3.11.4 y Django 4.2.3

Para empezar primero clonar el repositorio
```
git clone https://gitlab.com/fercopa/nubi-challenge.git myproj
cd myproj
```
Para levantar los servicios, este cuenta con un `docker-compose.yml` por lo que podemos correr este comando

```
docker-compose up
```

### Variables de entorno
Para definir varialbes de entorno, se usa el archivo .env que será usada por los dockers y el settings de la app.
Por lo general se sube una plantilla, sin dar valores a las variables. En este caso tienen valores para simplificar el despliegue local. Estas variables luego pueden ser agregadas a los servicios como AWS o GCP.

### Administradores
Al levanar los servicios automáticamente crea administradores configurados en el settings con una clave maestra `DJANGO_ADMIN_PASSWORD` definidas en el .env
```python
# settings.py
ADMINS = (
    ("admin", "admin@mail.com"),
)
# ...
```
Para ello cuenta con un command de django `initadmis` que se corre por primera vez cuando la base de datos está vacía. El código fuente se encuentra en `users/magements/commands/initadmins.py`
También se puede correr manualmente
```
python manage.py initadmins
```

### Usuarios mockeados
Al igual que el initadmis, cuenta con un command `load_mock_data` y que es encuenta en el mismo directorio de `initadmins`
Para correrlo manualmente
```
python manage.py load_mock_data
```

## Documentación de API
La documentación de la API se puede acceder una vez tengamos los servicios levantados.

[Documentación API](http://localhost:8000/api/v1/docs/#/)

## Correr localmente
Para correr localmente, se podría crear un entorno virtual y luego configurar la base de datos en settings o crear un nuevo settings local
```
python -m venv venv
pip install -r requirements.txt
```
### settings
```python
# ... Other settings
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
# ...
```

### Testing
Para correr los testing, debemos tener los servicios levantados y correr
```
docker exec django-nubi pytest tests
```

Si se tiene localmente instalado, bastaria con correr
```
pytest tests
```
